import bpy

def list_image_users_by_name(name=None):
    
    if name==None:
        return False
    
    im = bpy.data.images.get(name)
    if im == None:
        return False
    
    for ng in bpy.data.node_groups:
        for node in ng.nodes:
            if not node.type=='TEX_IMAGE':
                continue
            if not node.image==im:
                continue
            yield 'NodeGroup: '+ng.name
    
    for m in bpy.data.materials:
        if not hasattr(m, 'node_tree'):
            continue
        if not hasattr(m.node_tree, 'nodes'):
            continue
        for node in m.node_tree.nodes:
            if not node.type=='TEX_IMAGE':
                continue
            if not node.image==im:
                continue
            yield 'Material: '+m.name

def list_nodegroup_users_by_name(name=None):
    
    if name==None:
        return False
    
    nt = bpy.data.node_groups.get(name)
    if nt == None:
        return False
    
    for ng in bpy.data.node_groups:
        for node in ng.nodes:
            if not node.type=='GROUP':
                continue
            if not node.node_tree==nt:
                continue
            yield 'NodeGroup: '+ng.name
    
    for m in bpy.data.materials:
        if not hasattr(m, 'node_tree'):
            continue
        if not hasattr(m.node_tree, 'nodes'):
            continue
        for node in m.node_tree.nodes:
            if not node.type=='GROUP':
                continue
            if not node.node_tree==nt:
                continue
            yield 'Material: '+m.name

def list_material_users_by_name(name=None):
    
    if name==None:
        return False
    
    mat = bpy.data.materials.get(name)
    if mat == None:
        return False
    
    for ob in bpy.data.objects:
        if not ob.material_slots:
            continue
        for ms in ob.material_slots:
            if not ms.material==mat:
                continue
            yield 'Object: '+ob.name