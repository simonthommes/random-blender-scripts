import bpy
import numpy as np

image = bpy.data.images['<image_name>']

px = image.pixels[:]
r = np.array(px[::4])
g = np.array(px[1::4])
b = np.array(px[2::4])
a = np.array(px[3::4])

image.pixels = np.ravel([r,g,b,a],'F')