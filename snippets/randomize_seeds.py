import bpy
import numpy as np

name = 'RIG-salt_stick'

for ob in bpy.data.objects:
	if ob.type != 'ARMATURE':
			continue
	if not ob.name.startswith(name):
		continue
	for bone in ob.pose.bones:
		if bone.name.startswith('Properties_'):
			break
		bone = None
	if not bone:
		continue
	path = f'pose.bones["{bone.name}"]["Seed"]'
	if ob.animation_data:
		if ob.animation_data.action:
			fcurve = ob.animation_data.action.fcurves.find(path)
			if fcurve:
				ob.animation_data.action.fcurves.remove(fcurve)
	bone['Seed'] = np.random.randint(0, 100)
	ob.keyframe_insert(data_path=path, frame=bpy.context.scene.frame_current)
