import bpy

obj_from = bpy.context.active_object
obj_to_list = [ob for ob in bpy.context.selected_objects if not ob==obj_from]

for obj_to in obj_to_list:
    for vcol_from in obj_from.data.vertex_colors:
        vcol_to = obj_to.data.vertex_colors.get(vcol_from.name)
        if not vcol_to:
            vcol_to = obj_to.data.vertex_colors.new(name = vcol_from.name)
        for loop in obj_to.data.loops:
            vcol_to.data[loop.index].color = vcol_from.data[loop.index].color