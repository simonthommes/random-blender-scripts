import bpy

def draw_multi_object_uv_selection(self, context):
	''' UI element of warning and tool to fix unmatching UV selection for multi-object uv editing
	'''
	if not context.mode == 'EDIT_MESH':
		return
	me = context.edit_object.data

	layout = self.layout

	active_name = me.uv_layers.active.name

	flag = False

	for ob in context.selected_editable_objects:
		if not ob.type == 'MESH':
			continue
		if not ob.data.uv_layers.active:
			continue
		if not ob.data.uv_layers.active.name == active_name:
			flag = True
	if not flag:
		return
	row = layout.row()
	row.alert = True
	row.operator('uv_tools.multi_object_match_uvs',text='Match Selection', icon='ERROR')

class UVT_PT_uv_tools_panel(bpy.types.Panel):
	''' UI panel for UV tools
	'''
	bl_label = "UV Tools"
	bl_space_type = "IMAGE_EDITOR"
	bl_region_type = 'UI'
	bl_category = "UV Tools"

	def draw(self, context):
		layout = self.layout
		row = layout.row()
		row.operator('uv_tools.uv_copy')
		row.operator('uv_tools.uv_paste')
		row = layout.row()
		row.operator('uv_tools.relax_uvs').lock_axis = 'NONE'
		row = layout.row()
		row.operator('uv_tools.relax_uvs', text='Relax U').lock_axis = 'Y'
		row.operator('uv_tools.relax_uvs', text='Relax V').lock_axis = 'X'

		layout.operator('uv_tools.line_straighten')
		layout.operator('uv_tools.randomize')
		return


classes = [
	UVT_PT_uv_tools_panel,
	]

def register():
	for c in classes:
		bpy.utils.register_class(c)

	bpy.types.IMAGE_HT_header.append(draw_multi_object_uv_selection)

def unregister():
	for c in classes:
		bpy.utils.unregister_class(c)

	bpy.types.IMAGE_HT_header.remove(draw_multi_object_uv_selection)
