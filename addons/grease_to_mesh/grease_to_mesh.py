import bpy
import bmesh
from mathutils import Vector
import numpy as np

'''
TODO

- Do proper animation data transfer:
    - CURRENT - parent generated object to source stroke mesh
    - BETTER - mute transforms, parenting, constraints before conversion and transfer afterwards
- Handle and assign Materials
- investigate context poll faults on gpencil.convert

'''


class GPTOM_Settings(bpy.types.PropertyGroup):
    curve_thickness: bpy.props.FloatProperty(
        name = 'Thickness Factor',
        description = "Facor for line thickness on top of stroke strength",
        default = 1.,
        min = 0.,
    )
    curve_res: bpy.props.IntProperty(
        name = 'Curve Resolution',
        description = "Geometry resolution of the generated curve",
        default = 12,
        min = 0,
        max = 32,
    )
    planar_faces: bpy.props.BoolProperty(
        name = 'Planar Faces',
        description = 'Toggle whether to flatten generated faces',
        default = True,
    )
    fill_thickness: bpy.props.FloatProperty(
        name = 'Fill Thinkess',
        description = "Thickness of the filled in areas",
        default = .01,
        min = 0.,
        subtype = 'DISTANCE',
    )
    bevel_amount: bpy.props.FloatProperty(
        name = 'Bevel Amount',
        description = "Bevel amount of the filled areas",
        default = 50,
        min = 0,
        max = 100,
        subtype = 'PERCENTAGE',
    )
    bevel_segments: bpy.props.IntProperty(
        name = 'Bevel Segments',
        description = "Bevel segments of the filled areas",
        default = 2,
        min = 0,
        max = 32,
    )
    remesh_lines: bpy.props.BoolProperty(
        name = 'Remesh Lines',
        description = 'Toggle whether to remesh the generated line geometry',
        default = True,
    )
    remesh_fills: bpy.props.BoolProperty(
        name = 'Remesh Fills',
        description = 'Toggle whether to remesh the generated filled geometry',
        default = False,
    )
    res_factor: bpy.props.FloatProperty(
        name = 'Mesh Resolution Factor',
        description = "Factor for the generated mesh's resolution in arbitrary units",
        default = 1.,
        soft_min = 0.,
        soft_max = 2.,
    )
    smoothing: bpy.props.BoolProperty(
        name = 'Smoothing',
        description = 'Whether to use smoothing on the generated mesh',
        default = True,
    )
    smoothing_steps: bpy.props.IntProperty(
        name = 'Smoothing Steps',
        description = "Amount of smoothing steps on the mesh",
        default = 10,
        min = 0,
        soft_max = 20,
    )
    apply_mods: bpy.props.BoolProperty(
        name = 'Convert to Mesh',
        description = 'Whether to convert the generated curves with modifiers into a mesh as the final step',
        default = True,
    )
    key_type: bpy.props.EnumProperty(
        name = "Key Type",
        description = "Select the method to key the visibility of the generated objects",
        items = [
            ('SCALE', "Scale", "Key the obbjects' scale to 0 to make them vanish"),
            ('VISIBILITY', "Visibility", "Key the objects' viewport and render visibility"),
        ],
        default='SCALE'
    )
    stroke_layer: bpy.props.EnumProperty(
        name = "Stroke Layer",
        description = "Select which stroke layer to use for the conversion",
        items = [
            ('ACTIVE', "Active", "Convert the currently active stroke layer"),
            ('ALL', "All", "Convert all available stroke layers"),
            ('FLATTEN', "Flatten", "Flatten all available stroke layers into one"),
        ],
        default='FLATTEN'
    )


class GPTOM_PT_grease_to_mesh(bpy.types.Panel):
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_label = "Grease to Mesh"
    bl_category = 'Misc'
    
    def draw(self, context):
        layout = self.layout
        if not context.object:
            layout.label(text='No active object')
            return
        if not context.object.type == 'GPENCIL':
            layout.enabled = False
        
        settings = context.scene.grease_to_mesh_settings
        
        box = layout.box()
        box.label(text='Line Settings')
        box.prop(settings, 'curve_thickness')
        box.prop(settings, 'curve_res')
        
        box = layout.box()
        box.label(text='Fill Settings')
        box.prop(settings, 'planar_faces')
        box.prop(settings, 'fill_thickness')
        box.prop(settings, 'bevel_amount')
        box.prop(settings, 'bevel_segments')
        
        box = layout.box()
        box.label(text='Remesh Settings')
        row = box.row()
        row.prop(settings, 'remesh_lines', text='Lines')
        row.prop(settings, 'remesh_fills', text='Fills')
        box.prop(settings, 'res_factor')
        box.prop(settings, 'smoothing')
        row = box.row()
        row.active = settings.smoothing
        row.prop(settings, 'smoothing_steps')
        layout.alignment = 'RIGHT'
        layout.prop(settings, 'apply_mods')
        layout.prop(settings, 'key_type')
        layout.prop(settings, 'stroke_layer')
        layout.operator('grease_to_mesh.convert_current', icon='GP_SELECT_STROKES')
        layout.operator('grease_to_mesh.convert_range', icon='GP_MULTIFRAME_EDITING')
        
def add_modifiers(context, settings, modifiers):
    ''' Adds needed modifiers to a given modifier stack.
    '''
    
    mod = modifiers.new(name = 'Remesh', type = 'REMESH')
    mod.voxel_size = .001/settings.res_factor
    mod.adaptivity = .2
    mod.use_smooth_shade = True
    mod = modifiers.new(name = 'Weld', type = 'WELD')
    mod.mode = 'CONNECTED'
    mod.merge_threshold = .0008/settings.res_factor
    if settings.smoothing:
        mod = modifiers.new(name = 'Smooth', type = 'SMOOTH')
        mod.factor = .2
        mod.iterations = 10
    return

def key_visibility(obj, vis, frame):
    ''' Keys an objects visibility on a frame.
    '''
    obj.hide_render = not vis
    obj.hide_viewport = not vis
    obj.keyframe_insert(data_path='hide_render', frame=frame)
    obj.keyframe_insert(data_path='hide_viewport', frame=frame)
    return

def key_scale(obj, scl, frame):
    ''' Keys an objects scale on a frame.
    '''
    obj.scale = [scl]*3
    obj.keyframe_insert(data_path='scale', frame=frame, index=-1)
    for fcurve in obj.animation_data.action.fcurves:
        if fcurve.data_path == 'scale':
            kf = fcurve.keyframe_points[-1]
            kf.interpolation = 'CONSTANT'
    return

def stroke_cleanup(context, settings, stroke_object, stroke_type):
    ''' Remove strokes dependiong on the stroke type regarding their material properties in all stroke layers.
    '''
    for layer in stroke_object.data.layers:
        for frame in layer.frames:
            for stroke in frame.strokes:
                if stroke_type == 'LINES':
                    type_check = stroke_object.material_slots[stroke.material_index].material.grease_pencil.show_stroke
                elif stroke_type == 'FILLS':
                    type_check = stroke_object.material_slots[stroke.material_index].material.grease_pencil.show_fill
                if not type_check:
                    frame.strokes.remove(stroke)
    return
    
def flatten_stroke_layers(context, settings, stroke_object):
    ''' Returns a temporary grease pencil object with flattened stroke
        layers from a source object.
    '''
    # remove layers that are set to hidden
    for layer in stroke_object.data.layers:
        if layer.hide == True:
            stroke_object.data.layers.remove(layer)
    
    # add keyframes to preserve animation
    
    keyframes = []
    
    for layer in stroke_object.data.layers:
        keyframes += [f.frame_number for f in layer.frames]
    keyframes = list(dict.fromkeys(keyframes))
    
    for layer in stroke_object.data.layers:
        frame_map = {}
        for gp_frame in layer.frames:
            frame_map[gp_frame.frame_number] = gp_frame
        
        for frame in keyframes:
            if frame in frame_map.keys():
                continue
            nearest = min(frame_map.keys())
            if frame<nearest:
                new_frame = layer.frames.new(frame)
            else:
                for f in frame_map.keys():
                    if f<=frame and f>nearest:
                        nearest = f
                    
                new_frame = layer.frames.copy(frame_map[nearest])
                new_frame.frame_number = frame
    
    bpy.ops.gpencil.recalc_geometry({'active_object': stroke_object, 'object': stroke_object, 'selected_objects': [stroke_object], 'selected_editable_objects': [stroke_object]})
    
    # flatten stroke layers
    while len(stroke_object.data.layers)>1:
        stroke_object.data.layers.active_index = len(stroke_object.data.layers)-1
        bpy.ops.gpencil.layer_merge({'active_object': stroke_object, 'object': stroke_object})
    stroke_object.data.layers.active.info = 'Flat'
    
    return

def apply_mesh_to_new_object(context, object, name):
    ''' Returns a mesh object with applied modifiers.
    '''
    depsgraph = context.evaluated_depsgraph_get()
    object_evaluated = object.evaluated_get(depsgraph)
    object.modifiers.clear()
    mesh = bpy.data.objects.new(name, bpy.data.meshes.new_from_object(object_evaluated))
            
    return mesh

def convert_lines(context, settings, stroke_object):
    obj_pre = context.evaluated_depsgraph_get().objects.values()
    
    override = context.copy()
    override['active_object'] = stroke_object
    override['object'] = stroke_object
    override['selected_editable_objects'] = [stroke_object]
    override['selected_objects'] = [stroke_object]
    
    bpy.ops.gpencil.convert(override,
                            type = "CURVE",
                            bevel_depth = .008*settings.curve_thickness,
                            bevel_resolution = settings.curve_res,
                            )
    obj_post = context.evaluated_depsgraph_get().objects.values()
    for ob in obj_pre:
        obj_post.remove(ob)
    curve_object = bpy.data.objects.get(obj_post[0].name)
    curve_object.data.use_fill_caps = True
    return curve_object

def convert_fills(context, settings, stroke_object):
    # convert to path
    
    bpy.ops.gpencil.recalc_geometry({'active_object': stroke_object, 'object': stroke_object, 'selected_objects': [stroke_object], 'selected_editable_objects': [stroke_object]})
    
    override = context.copy()
    override['active_object'] = stroke_object
    override['object'] = stroke_object
    override['selected_editable_objects'] = [stroke_object]
    override['selected_objects'] = [stroke_object]
    
    obj_pre = context.evaluated_depsgraph_get().objects.values()
    bpy.ops.gpencil.convert(override,
                            type='CURVE')
    obj_post = context.evaluated_depsgraph_get().objects.values()
    for ob in obj_pre:
        obj_post.remove(ob)
    curve_object = bpy.data.objects.get(obj_post[0].name)
    
    # make all cyclic
    for spline in curve_object.data.splines:
        spline.use_cyclic_u = True
    
    # convert to mesh
    curve_object.data.resolution_u = 1
    bpy.ops.object.convert({'active_object': curve_object, 'object': curve_object, 'selected_editable_objects': [curve_object]}, target='MESH')
    
    # make faces
    bm = bmesh.new()
    bm.from_mesh(curve_object.data)
    bmesh.ops.contextual_create(bm, geom=list(bm.edges))
    if settings.planar_faces:
        bmesh.ops.planar_faces(bm, faces=bm.faces, iterations=1, factor=1.)
    bm.to_mesh(curve_object.data)
    del(bm)
    
    # add modifiers
    mod = curve_object.modifiers.new(name = 'Solidify', type = 'SOLIDIFY')
    mod.offset = 0
    mod.thickness = settings.fill_thickness
    if settings.bevel_segments>0 and settings.bevel_amount>0:
        mod = curve_object.modifiers.new(name = 'Bevel', type = 'BEVEL')
        mod.offset_type = 'PERCENT'
        mod.segments = settings.bevel_segments
        mod.width = settings.bevel_amount
        mod.use_clamp_overlap = False
        mod = curve_object.modifiers.new(name = 'Smooth', type = 'SMOOTH')
        mod.factor = .2
        mod.iterations = 10
        for face in curve_object.data.polygons:
            face.use_smooth = True
    
    return curve_object
    
def grease_to_mesh_core(context, settings, stroke_object, name, parent_object=None, stroke_type='LINES'):
    ''' Core functionality of converting a GP object into mesh.
    '''
    
    
    if stroke_type == 'LINES':
        curve_object = convert_lines(context, settings, stroke_object)
        remesh = settings.remesh_lines
    elif stroke_type == 'FILLS':
        curve_object = convert_fills(context, settings, stroke_object)
        remesh = settings.remesh_fills
    
    if remesh:
        add_modifiers(context, settings, curve_object.modifiers)
    
    ob = bpy.data.objects.get(name)
    if ob:
        bpy.data.objects.remove(ob)
        
    if settings.apply_mods:
        mesh_object = apply_mesh_to_new_object(context, curve_object, name)
    else:
        mesh_object = curve_object
    mesh_object.name = name
    if not parent_object:
        parent_object = stroke_object
    bpy.ops.object.parent_set({'active_object': parent_object, 'object': parent_object, 'selected_editable_objects': [mesh_object]})
    
    return curve_object, mesh_object

def active_stroke_layer_to_mesh_current(context, settings, stroke_object, parent_object=None, stroke_type='LINES'):
    ''' Converts the active stroke layer of a given GP object into mesh
        according to settings.
    '''
    # assure stroke data
    frame_map = {}
    for gp_frame in stroke_object.data.layers.active.frames:
        frame_map[gp_frame.frame_number] = gp_frame
    
    frame_current = context.scene.frame_current
    
    if frame_current in frame_map.keys():
        frame = frame_map[frame_current]
    
    invalid = False
    nearest = min(frame_map.keys())
    if frame_current<nearest:
        invalid = True
    else:
        for f in frame_map.keys():
            if f<=frame_current and f>nearest:
                nearest = f
    if (len(frame_map[nearest].strokes))==0:
        invalid = True
    
    if invalid:
        print(f'Layer {stroke_object.data.layers.active.info} contains no stroke data on the current frame. Skipping.')
        return
    
    coll_parent = context.scene.collection
    name = stroke_object.name+'.'+stroke_object.data.layers.active.info+'-'+str(context.scene.frame_current).zfill(5)
    
    curve_object, mesh_object = grease_to_mesh_core(context, settings, stroke_object, name, parent_object, stroke_type)
    
    if not mesh_object in coll_parent.objects.values():
        coll_parent.objects.link(mesh_object)
    if not mesh_object == curve_object:
        bpy.data.objects.remove(curve_object)
    return


def active_stroke_layer_to_mesh_range(context, settings, stroke_object, parent_object=None, stroke_type='LINES'):
    ''' Converts the active stroke layer of a given GP object into mesh
        according to settings for the whole frame range.
    '''
    # Create/integrate target collection hierarchy
    coll_parent = context.scene.collection
    
    coll_stroke = bpy.data.collections.get(stroke_object.name)
    if not coll_stroke:
        coll_stroke = bpy.data.collections.new(stroke_object.name)
        coll_parent.children.link(coll_stroke)
    
    name = stroke_object.name+'.'+stroke_object.data.layers.active.info
    coll = coll_stroke.children.get(name)
    if not coll:
        coll = bpy.data.collections.new(name)
    if not coll in coll_stroke.children.values():
        coll_stroke.children.link(coll)
    
    # Run though frames and create meshes for GP keyframes
    
    
    frame_map = {}
    for gp_frame in stroke_object.data.layers.active.frames:
        frame_map[gp_frame.frame_number] = gp_frame
    
    keys = frame_map.keys()
    
    frame = context.scene.frame_start
    while frame <= context.scene.frame_end:
        name_frame = name+'_'+str(frame).zfill(5)
        if frame in keys:
            if 'mesh_object' in locals():
                if settings.key_type == 'SCALE':
                    key_scale(mesh_object, False, frame)
                elif settings.key_type == 'VISIBILITY':
                    key_visibility(mesh_object, False, frame)
            if len(frame_map[frame].strokes)==0:
                frame += 1
                continue
            print(f'Converting frame {frame} layer: {stroke_object.data.layers.active.info}')
            context.scene.frame_set(frame)
            '''
            for key in context.__dir__():
                item = getattr(context, key)
                if not item or key in ['visible_objects', 'editable_objects', 'selectable_objects']:
                    continue
                print(key, item)
            '''
            curve_object, mesh_object = grease_to_mesh_core(context, settings, stroke_object, name_frame, parent_object, stroke_type)
            
            if settings.key_type == 'SCALE':
                key_scale(mesh_object, False, frame-1)
                key_scale(mesh_object, True, frame)
            elif settings.key_type == 'VISIBILITY':
                key_visibility(mesh_object, False, frame-1)
                key_visibility(mesh_object, True, frame)
            
            if not mesh_object in coll.objects.values():
                coll.objects.link(mesh_object)
            if not mesh_object == curve_object:
                bpy.data.objects.remove(curve_object)
        frame += 1
    return


def grease_to_mesh(context, settings, stroke_object, current=False):
    ''' Manages what to convert and how and creates object duplicates to preserve the source.
    '''
    active_stroke_layer_to_mesh = active_stroke_layer_to_mesh_current if current else active_stroke_layer_to_mesh_range
    
    name = stroke_object.name
    for stroke_type in ['FILLS', 'LINES']:
        stroke_object_copy = stroke_object.copy()
        stroke_object_copy.data = stroke_object_copy.data.copy()
        context.scene.collection.objects.link(stroke_object_copy)
        stroke_object_copy.name = name+'.'+stroke_type
        
        stroke_cleanup(context, settings, stroke_object_copy, stroke_type)
        
        if settings.stroke_layer == 'ACTIVE':
            active_stroke_layer_to_mesh(context, settings, stroke_object_copy, parent_object=stroke_object, stroke_type=stroke_type)
        elif settings.stroke_layer == 'ALL':
            for i in range(len(stroke_object_copy.data.layers)):
                stroke_object_copy.data.layers.active_index = i
                if stroke_object_copy.data.layers.active.hide:
                    continue
                if len(stroke_object_copy.data.layers.active.frames)==0:
                    continue
                active_stroke_layer_to_mesh(context, settings, stroke_object_copy, parent_object=stroke_object, stroke_type=stroke_type)
        elif settings.stroke_layer == 'FLATTEN':
            flatten_stroke_layers(context, settings, stroke_object_copy)
            active_stroke_layer_to_mesh(context, settings, stroke_object_copy, parent_object=stroke_object, stroke_type=stroke_type)
        
        context.scene.collection.objects.unlink(stroke_object_copy)
        bpy.data.objects.remove(stroke_object_copy)
        del(stroke_object_copy)
    stroke_object.name = name
    return


class GPTOM_OT_convert_current(bpy.types.Operator):
    """ """
    bl_idname = "grease_to_mesh.convert_current"
    bl_label = "Convert Current Frame"
    bl_options = {'REGISTER', 'UNDO', 'INTERNAL'}

    def execute(self, context):
        settings = context.scene.grease_to_mesh_settings
        grease_to_mesh(context, settings, context.object, current=True)
        return {'FINISHED'}


class GPTOM_OT_convert_range(bpy.types.Operator):
    """ """
    bl_idname = "grease_to_mesh.convert_range"
    bl_label = "Convert Frame Range"
    bl_options = {'REGISTER', 'UNDO', 'INTERNAL'}

    def execute(self, context):
        settings = context.scene.grease_to_mesh_settings
        frame = context.scene.frame_current
        grease_to_mesh(context, settings, context.object, current=False)
        context.scene.frame_current = frame
        print('Done Converting!')
        return {'FINISHED'}


classes = (
    GPTOM_Settings,
    GPTOM_PT_grease_to_mesh,
    GPTOM_OT_convert_current,
    GPTOM_OT_convert_range,
)

def register():
    for c in classes:
        bpy.utils.register_class(c)
    bpy.types.Scene.grease_to_mesh_settings = bpy.props.PointerProperty(type=GPTOM_Settings)

def unregister():
    for c in classes:
        bpy.utils.unregister_class(c)
    del bpy.types.Scene.grease_to_mesh_settings

if __name__ == "__main__":
    register()