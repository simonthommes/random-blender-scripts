import bpy
from . import ui, ops, settings

bl_info = {
	"name": "Baking Buddy",
	"author": "Simon Thommes",
	"version": (0,1),
	"blender": (3, 3, 0),
	"location": "Image Editor > Sidebar > Baking Buddy",
	"description": "Toolset to simplify baking actions.",
	"category": "Workflow",
}

modules = [settings, ops, ui]

def register():
	for m in modules:
		m.register()

def unregister():
	for m in modules:
		m.unregister()
