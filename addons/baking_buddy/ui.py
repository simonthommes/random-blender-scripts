import bpy

class BAKB_PT_baking_buddy_panel_main(bpy.types.Panel):
	bl_space_type = "IMAGE_EDITOR"
	bl_region_type = 'UI'
	bl_category = "Baking Buddy"
	bl_idname = "BAKB_PT_baking_buddy_panel_main"
	bl_label = "Baking Buddy"

	def draw(self, context):
		layout = self.layout

		settings = context.scene.baking_buddy_settings

		row = layout.row()
		img = context.area.spaces.active.image
		row.template_ID_preview(context.area.spaces.active, 'image', new="image.new", open="image.open")

		col = layout.column()
		col.operator('baking_buddy.bake')
		col.operator('baking_buddy.bake_from_highres')

		# Projection Settings
		col = layout.column()
		col.use_property_split = True
		cbk = context.scene.render.bake
		col.prop(cbk, "cage_extrusion", text="Extrusion")
		col.prop(cbk, "max_ray_distance")
		col.prop(cbk, "use_clear")
		col.prop(cbk, "margin")

		cbk = context.scene.cycles
		
		layout.prop(cbk, "bake_type")

		return

class BAKB_PT_baking_buddy_panel_bake_settings(bpy.types.Panel):
	''' UI panel for Baking Buddy
	'''
	bl_space_type = "IMAGE_EDITOR"
	bl_region_type = 'UI'
	bl_category = "Baking Buddy"
	bl_idname = "BAKB_PT_baking_buddy_panel_bake_settings"
	bl_parent_id = "BAKB_PT_baking_buddy_panel_main"
	bl_label = "Bake Settings"
	bl_options = {"DEFAULT_CLOSED"}
	
	def draw(self, context):
		layout = self.layout
		cbk = context.scene.render.bake
		cscene = context.scene.cycles
		settings = context.scene.baking_buddy_settings

		layout.prop(settings, 'sample_count')

		col = layout.column()

		if cscene.bake_type == 'NORMAL':
			col.prop(cbk, "normal_space", text="Space")

			sub = col.column(align=True)
			sub.prop(cbk, "normal_r", text="Swizzle R")
			sub.prop(cbk, "normal_g", text="G")
			sub.prop(cbk, "normal_b", text="B")

		elif cscene.bake_type == 'COMBINED':

			col = layout.column(heading="Lighting", align=True)
			col.prop(cbk, "use_pass_direct")
			col.prop(cbk, "use_pass_indirect")

			col = layout.column(heading="Contributions", align=True)
			col.active = cbk.use_pass_direct or cbk.use_pass_indirect
			col.prop(cbk, "use_pass_diffuse")
			col.prop(cbk, "use_pass_glossy")
			col.prop(cbk, "use_pass_transmission")
			col.prop(cbk, "use_pass_emit")

		elif cscene.bake_type in {'DIFFUSE', 'GLOSSY', 'TRANSMISSION'}:
			col = layout.column(heading="Contributions", align=True)
			col.prop(cbk, "use_pass_direct")
			col.prop(cbk, "use_pass_indirect")
			col.prop(cbk, "use_pass_color")

		return


classes = [
	BAKB_PT_baking_buddy_panel_main,
	BAKB_PT_baking_buddy_panel_bake_settings,
	]

def register():
	for c in classes:
		bpy.utils.register_class(c)

def unregister():
	for c in classes:
		bpy.utils.unregister_class(c)
