import bpy

class BAKB_Settings(bpy.types.PropertyGroup):
    target_image: bpy.props.PointerProperty(
        name = 'Target Image',
        description = "Image texture that will be baked to",
        type = bpy.types.Image
    )
    sample_count: bpy.props.IntProperty(
        name = 'Sample Count',
        default = 32,
        min = 1
    )

def register():
    bpy.utils.register_class(BAKB_Settings)
    bpy.types.Scene.baking_buddy_settings = bpy.props.PointerProperty(type=BAKB_Settings)

def unregister():
    bpy.utils.unregister_class(BAKB_Settings)
    del bpy.types.Scene.baking_buddy_settings
