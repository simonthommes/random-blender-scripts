import bpy
from . import utils
from . import settings

def add_image_to_material(material, image):
    ''' Adds an active image texture node to a material's node tree.
    '''
    if not material.node_tree:
        return False
    
    active_node = material.node_tree.nodes.active
    if active_node:
        active_node.name = f'ACT:{active_node.name}'
    
    tex_node = material.node_tree.nodes.new('ShaderNodeTexImage')
    tex_node.name = 'BAKB_bake_output'
    material.node_tree.nodes.active = tex_node
    
    tex_node.image = image
    
    return

def remove_image_from_material(material):
    ''' Removes the temporary image texture node from a material's node tree and tries to restore the priviously active one.
    '''
    if not material.node_tree:
        return False
    
    for n in material.node_tree.nodes:
        if n.name.startswith('ACT:'):
            n.name = n.name[4:]
            material.node_tree.nodes.active = n
            break
    
    tex_node = material.node_tree.nodes.get('BAKB_bake_output')
    if not tex_node:
        return
    material.node_tree.nodes.remove(tex_node)
    return

def bake(context, image, objects, from_highres=False):
	c_render = bpy.context.scene.render
	engine = c_render.engine
	c_render.engine = 'CYCLES'

	use_highres = c_render.bake.use_selected_to_active
	c_render.bake.use_selected_to_active = from_highres
	c_render.use_bake_multires = False
	
	# select active UVMap
	
	if from_highres:
		# find highres pairings
		object_map = dict()
		for ob in objects:
			ob_highres_list = []
			ob_highres = bpy.data.objects.get(f"{ob.name}.highres")
			if not ob_highres:
				objects.remove(ob)
				continue
			ob_highres_list.append(ob_highres)
			for i in range(999):
				ob_highres = bpy.data.objects.get(f"{ob.name}.highres.{str(i+1).zfill(3)}")
				if not ob_highres:
					continue
				ob_highres_list.append(ob_highres)
			object_map[ob] = ob_highres_list
	
	# add active image texture node to all materials on target objects
	done = set()
	for ob in objects:
		for m_slot in ob.material_slots:
			if not m_slot.material:
				continue
			if m_slot.material in done:
				continue
			add_image_to_material(m_slot.material, image)
			done.add(m_slot.material)
	
	# autosmooth off
	# link all objects to scene colleciton and ensure renderability 

	
	# bake
	if from_highres:
		c_render.bake.use_selected_to_active = True #TODO Remove(include in poll)
		for i, ob in enumerate(objects):
			
			print(f'BAKING {ob.name} ({i+1}/{len(objects)})')
			
			auto_smooth = ob.data.use_auto_smooth
			ob.data.use_auto_smooth = False
			
			c_override = {'object': ob,
							'active_object': ob,
							'selected_objects': object_map[ob],
							'selected_editable_objects': object_map[ob]}
			bpy.ops.object.bake(c_override, type=context.scene.cycles.bake_type)
		
			ob.data.use_auto_smooth = auto_smooth
	else:
		c_render.bake.use_selected_to_active = False
		bpy.ops.object.bake(type=context.scene.cycles.bake_type)
	
	
	# remove texture nodes
	for m in bpy.data.materials:
		remove_image_from_material(m)
	
	print('DONE')
	
	#restore settings
	c_render.engine = engine
	c_render.bake.use_selected_to_active = use_highres


class BAKB_OT_bake_from_highres(bpy.types.Operator):
	"""
	"""
	bl_idname = "baking_buddy.bake_from_highres"
	bl_label = "Bake from Highres"
	bl_options = {"REGISTER", "UNDO"}
	bl_description = "Bake information of all selected objects from their highres version mapped by name + suffix to the current image"
	
	@classmethod
	def poll(cls, context):
		return bool(context.selected_objects)

	def execute(self, context):
		settings = context.scene.baking_buddy_settings

		samples = context.scene.cycles.samples

		context.scene.cycles.samples = settings.sample_count
		bake(context, context.area.spaces.active.image, context.selected_objects, from_highres=True)

		context.scene.cycles.samples = samples

		return {'FINISHED'}

class BAKB_OT_bake(bpy.types.Operator):
	"""
	"""
	bl_idname = "baking_buddy.bake"
	bl_label = "Bake"
	bl_options = {"REGISTER", "UNDO"}
	bl_description = "Bake information of all selected objects to the current image"
	
	@classmethod
	def poll(cls, context):
		return bool(context.selected_objects)

	def execute(self, context):
		settings = context.scene.baking_buddy_settings

		samples = context.scene.cycles.samples

		context.scene.cycles.samples = settings.sample_count
		bake(context, context.area.spaces.active.image, context.selected_objects)

		context.scene.cycles.samples = samples

		return {'FINISHED'}

classes = [
	BAKB_OT_bake_from_highres,
	BAKB_OT_bake
	]


def register():
	for c in classes:
		bpy.utils.register_class(c)

def unregister():
	for c in classes:
		bpy.utils.unregister_class(c)
