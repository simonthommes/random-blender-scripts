import bpy
from . import utils
from . import settings

class NOB_MT_input_menu(bpy.types.Menu):
    bl_label = "Find Input"
    bl_idname = "NOB_MT_input_menu"

    def draw(self, context):
        layout = self.layout
        settings = context.scene.node_buddy_settings

        nt = utils.get_context_nt(context)

        inputs = []
        for item in nt.interface.items_tree:
            if not hasattr(item, 'in_out'):
                continue
            if item.in_out == 'INPUT':
                inputs += [item]
        
        for input in inputs:
            op = layout.operator("node_buddy.find_input_nodes", text=input.name)
            op.socket_id = input.identifier
        return

class NOB_OT_find_input_nodes(bpy.types.Operator):
    """
    """
    bl_idname = "node_buddy.find_input_nodes"
    bl_label = "Find Input Nodes"
    bl_options = {"REGISTER", "UNDO"}
    bl_description = "Select all instances of the group input node where a specific input socket is used"

    socket_id: bpy.props.StringProperty(default='')
    
    @classmethod
    def poll(cls, context):
        if not hasattr(context.area.spaces.active, 'node_tree'):
            return False
        return bool(context.area.spaces.active.node_tree)
    
    def execute(self, context):
        settings = context.scene.node_buddy_settings

        if not self.socket_id:
            utils.set_context_nt(context, context.area.spaces.active.node_tree)
            bpy.ops.wm.call_menu(name=NOB_MT_input_menu.bl_idname)
            return {'FINISHED'}

        nt = utils.get_context_nt(context)

        linked_nodes = set()
        for l in nt.links:
            node = l.from_node
            if not node.type == 'GROUP_INPUT':
                continue
            if not l.from_socket.identifier == self.socket_id:
                continue
            linked_nodes.add(node)

        for n in nt.nodes:
            if n not in linked_nodes:
                n.select = False
                continue
            n.select = True

        utils.reset_context_nt(context)
        self.socket_id = ''

        return {'FINISHED'}
    
    def cancel(self, context):
        settings = context.scene.node_buddy_settings
        settings.context_node_tree = None

        print('CANCEL')



classes = [
    NOB_MT_input_menu,
    NOB_OT_find_input_nodes,
    ]


def register():
    for c in classes:
        bpy.utils.register_class(c)

def unregister():
    for c in classes:
        bpy.utils.unregister_class(c)
