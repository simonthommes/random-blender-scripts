import bpy

class NOB_Settings(bpy.types.PropertyGroup):
    context_nt_name: bpy.props.StringProperty(name = 'Context Node Tree', default = '')

def register():
    bpy.utils.register_class(NOB_Settings)
    bpy.types.Scene.node_buddy_settings = bpy.props.PointerProperty(type=NOB_Settings)

def unregister():
    bpy.utils.unregister_class(NOB_Settings)
    del bpy.types.Scene.node_buddy_settings
