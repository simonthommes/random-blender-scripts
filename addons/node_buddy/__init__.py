from . import ui, ops, settings

bl_info = {
	"name": "Node Buddy",
	"author": "Simon Thommes",
	"version": (0,1),
	"blender": (4, 2, 0),
	"location": "Node Editor > Sidebar > Node Buddy",
	"description": "Toolset to support node editing workflow.",
	"category": "Node",
}

modules = [settings, ops, ui]

def register():
	for m in modules:
		m.register()

def unregister():
	for m in modules:
		m.unregister()
