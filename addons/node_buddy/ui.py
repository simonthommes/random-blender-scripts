import bpy

class NOB_PT_NODE_buddy_panel_main(bpy.types.Panel):
	bl_space_type = "NODE_EDITOR"
	bl_region_type = 'UI'
	bl_category = "Node Buddy"
	bl_idname = "NOB_PT_NODE_buddy_panel_main"
	bl_label = "Node Buddy"

	def draw(self, context):
		layout = self.layout

		settings = context.scene.node_buddy_settings

		row = layout.row()
		nt = context.area.spaces.active.node_tree

		col = layout.column()
		col.operator('node_buddy.find_input_nodes')

		return

classes = [
	NOB_PT_NODE_buddy_panel_main,
	]

def register():
	for c in classes:
		bpy.utils.register_class(c)

def unregister():
	for c in classes:
		bpy.utils.unregister_class(c)
