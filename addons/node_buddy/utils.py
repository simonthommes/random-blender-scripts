import bpy

def set_context_nt(context, nt):
    settings = context.scene.node_buddy_settings
    if not nt:
        settings.context_nt_name = ''
        return
    settings.context_nt_name = nt.name
    return

def reset_context_nt(context):
    set_context_nt(context, None)
    return

def get_context_nt(context):
    settings = context.scene.node_buddy_settings
    if not settings.context_nt_name:
        return None
    nt = bpy.data.node_groups.get(settings.context_nt_name)
    return nt