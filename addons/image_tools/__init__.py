import bpy
from . import ui, ops

bl_info = {
	"name": "Image Tools",
	"author": "Simon Thommes",
	"version": (0,1),
	"blender": (3, 0, 0),
	"location": "Image Editor > Sidebar > Image Tools",
	"description": "Toolset to perform image processing actions directly from the image editor in Blender.",
	"category": "Workflow",
}

modules = [ops, ui]

def register():
    for m in modules:
        m.register()

def unregister():
    for m in modules:
        m.unregister()