import bpy
import numpy as np
import math
from datetime import datetime

def img_to_arr(img):
    ''' Converts blender image into a numpy array. Shape: (Row Index, Column Index, Channel)
    '''
    if not img:
        return None
    x, y = img.size
    pixels = np.empty(x * y << 2, dtype=np.float32)
    img.pixels.foreach_get(pixels)
    pixels = pixels.reshape((y, x, img.channels))
    return pixels

def img_from_arr(img, arr):
    ''' Writes color data from a numpy array into the pixel data of a blender image.
    '''
    if not img:
        return False
    if not math.prod(list(img.size[:])+[img.channels]) == math.prod(arr.shape):
        return False
    pixels = arr.flatten('C').astype(np.float32)
    img.pixels.foreach_set(pixels)
    return

def date_as_string(time=True):
    time = datetime.now()
    string = time.strftime("%Y-%m-%d")
    if time:
        string += time.strftime("_%H:%M:%S")
    return string
