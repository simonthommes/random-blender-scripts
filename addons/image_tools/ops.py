import bpy
from . import utils, utils_extra
import numpy as np
import time

def isolate_channel(arr, channel=0):
	return arr[:,:,channel]

def generate_sdf(arr, scale=10):
	start = time.time()
	arr = arr[:,:,0]
	grid = np.mgrid[0:arr.shape[0],0:arr.shape[1]]
	for i,j in zip(grid[0].flatten(),grid[1].flatten()):
		if not arr[i,j]==0:
			continue
		neighbors = []
		for i_2 in [-1,0,1]:
			for j_2 in [-1,0,1]:
				if i_2 == 0 and j_2 == 0:
					continue
				try:
					val = arr[i+i_2,j+j_2]
					neighbors.append(val)
				except:
					pass
		if max(neighbors)==0:
			continue
		dist_field = ((grid[0]-i)**2+(grid[1]-j)**2)**.5/scale
		arr = np.minimum(arr,dist_field)
	
	print('TIME ELAPSED: ', time.time()-start)
	
	return np.dstack((arr, arr, arr, np.full(arr.shape, 1)))

def generate_sdf_approx(arr, steps=10):
	arr = arr[:,:,0]
	grid = np.mgrid[0:arr.shape[0],0:arr.shape[1]]
	for s in range(steps):
		for i,j in zip(grid[0].flatten(),grid[1].flatten()):
			layer = s/steps
			if arr[i,j] <= layer:
				continue
			neighbors_str = []
			neighbors_diag = []
			for i_2 in [-1,0,1]:
				for j_2 in [-1,0,1]:
					if i_2 == 0 and j_2 == 0:
						continue
					try:
						val = arr[i+i_2,j+j_2]
						if abs(i_2)==abs(j_2):
							neighbors_diag.append(val)
						else:
							neighbors_str.append(val)
					except:
						pass
			min_str = min(neighbors_str)
			min_diag = min(neighbors_diag)
			min_val = min((min_str,min_diag))
			if min_val > layer:
				continue
			if min_str <= min_diag:
				arr[i,j] = min_val+1./steps
			else:
				arr[i,j] = min_val+(1./steps)*2.**0.5
	
	return np.dstack((arr, arr, arr, np.full(arr.shape, 1)))
	
class IMT_OT_create_snapshot(bpy.types.Operator):
	"""
	"""
	bl_idname = "image_tools.create_snapshot"
	bl_label = "Create Viewport Snapshot"
	bl_options = {"REGISTER", "UNDO"}
	
	@classmethod
	def poll(cls, context):
		return context.area.type == 'VIEW_3D'
	
	def execute(self, context):
		
		window = context.window
		area = context.area
		region = [r for r in area.regions if r.type == 'WINDOW'][0]
		
		space = area.spaces[0]
		border_bbox = (space.render_border_min_x, space.render_border_min_y, space.render_border_max_x, space.render_border_max_y)
		
		bbox = (window.x+area.x+int(region.width*border_bbox[0]),
				window.y+area.y+int(region.height*border_bbox[1]),
				window.x+area.x+int(region.width*border_bbox[2]),
				window.y+area.y+int(region.height*border_bbox[3]))
		
		name = f'snapshot_{utils.date_as_string()}'
		
		utils_extra.screen_print_to_image(name=name, bbox=bbox)
		return {'FINISHED'}
		
class IMT_OT_write_to_channel(bpy.types.Operator):
	"""
	"""
	bl_idname = "image_tools.write_to_channel"
	bl_label = "Write to Channel"
	bl_options = {"REGISTER", "UNDO"}

	@classmethod
	def poll(self, context):
		settings = context.scene.IMT_Settings

		img_src = context.area.spaces.active.image
		img_tgt = settings.target_image
		if not img_tgt:
			return False
	
		return img_src.size[:2] == img_tgt.size[:2]
	
	def execute(self, context):
		settings = context.scene.IMT_Settings

		img_src = context.area.spaces.active.image
		img_tgt = settings.target_image
		if not img_tgt:
			return {'CANCELLED'}
		
		arr_tgt = utils.img_to_arr(img_tgt)
		arr_src = utils.img_to_arr(img_src)
		
		channel = int(settings.channel_RGBA)
		arr_tgt = [isolate_channel(arr_tgt, channel=i) for i in range(4)]
		arr_src = isolate_channel(arr_src, channel=0)

		arr_tgt = np.dstack(tuple([arr_tgt[i] if i!=channel else arr_src for i in range(4)]))
		
		context.area.spaces.active.image = img_tgt
		if not utils.img_from_arr(img_tgt, arr_tgt):
			return {'CANCELLED'}
		else:
			return {'FINISHED'}

		return {'FINISHED'}

class IMT_OT_generate_sdf(bpy.types.Operator):
	"""
	"""
	bl_idname = "image_tools.generate_sdf"
	bl_label = "Generate SDF"
	bl_options = {"REGISTER", "UNDO"}
	
	radius: bpy.props.IntProperty(name='Radius', default=10, min=0, soft_max=1000)
	
	def invoke(self, context, event):
		wm = context.window_manager
		return wm.invoke_props_dialog(self)
	
	def execute(self, context):
		settings = context.scene.IMT_Settings
		img = context.area.spaces.active.image
		arr = utils.img_to_arr(img)
		
		arr = generate_sdf(arr, scale=self.radius)
		
		if not utils.img_from_arr(img, arr):
			return {'CANCELLED'}
		else:
			return {'FINISHED'}
			
class IMT_OT_generate_sdf_approx(bpy.types.Operator):
	"""
	"""
	bl_idname = "image_tools.generate_sdf_approx"
	bl_label = "Generate Approximate SDF"
	bl_options = {"REGISTER", "UNDO"}
	
	steps: bpy.props.IntProperty(name='Steps', default=10, min=0, soft_max=100)
	
	def invoke(self, context, event):
		wm = context.window_manager
		return wm.invoke_props_dialog(self)
	
	def execute(self, context):
		settings = context.scene.IMT_Settings
		img = context.area.spaces.active.image
		arr = utils.img_to_arr(img)
		
		arr = generate_sdf_approx(arr, steps=self.steps)
		
		if not utils.img_from_arr(img, arr):
			return {'CANCELLED'}
		else:
			return {'FINISHED'}
			
class IMT_OT_isolate_channel(bpy.types.Operator):
	"""
	"""
	bl_idname = "image_tools.isolate_channel"
	bl_label = "Isolate Channel"
	bl_options = {"REGISTER", "UNDO"}
	
	def execute(self, context):
		settings = context.scene.IMT_Settings
		img = context.area.spaces.active.image
		arr = utils.img_to_arr(img)
		
		channel = int(settings.channel_RGBA)
		arr = isolate_channel(arr, channel=channel)
		arr = np.dstack((arr, arr, arr, np.full(arr.shape, 1)))
		
		img = bpy.data.images.new(img.name+'_'+['R','G','B','A'][channel], *img.size)
		context.area.spaces.active.image = img
		if not utils.img_from_arr(img, arr):
			return {'CANCELLED'}
		else:
			return {'FINISHED'}

class IMT_OT_flip_image(bpy.types.Operator):
	"""
	"""
	bl_idname = "image_tools.flip_image"
	bl_label = "Flip Image"
	bl_options = {"REGISTER", "UNDO"}
	
	mode: bpy.props.EnumProperty(items = [
											("HORIZONTAL", "Horizontal", "Flip image horizontically"),
											("VERTICAL", "Vertical", "Flip image vertically"),])
	
	def execute(self, context):
		img = context.area.spaces.active.image
		arr = utils.img_to_arr(img)
		if self.mode == 'HORIZONTAL':
			arr = np.flip(arr, 1)
		else:
			arr = np.flip(arr, 0)
		if not utils.img_from_arr(img, arr):
			return {'CANCELLED'}
		else:
			return {'FINISHED'}

classes = [	IMT_OT_create_snapshot,
			IMT_OT_flip_image,
			IMT_OT_isolate_channel,
			IMT_OT_write_to_channel,
			IMT_OT_generate_sdf_approx,
			IMT_OT_generate_sdf,]


def register():
	for c in classes:
		bpy.utils.register_class(c)

def unregister():
	for c in classes:
		bpy.utils.unregister_class(c)
