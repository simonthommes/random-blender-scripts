import bpy

def draw_snapshot(self, context):
	layout = self.layout
	row = layout.row(align=True)
	row.operator('image_tools.create_snapshot', text='', icon='RESTRICT_RENDER_OFF')
	row.popover('IMT_PT_snapshot', text='')

class IMT_Settings(bpy.types.PropertyGroup):
	channel_RGBA: bpy.props.EnumProperty(default='0',
		items= [('0', 'R', 'Red', '', 0),
				('1', 'G', 'Green', '', 1),
				('2', 'B', 'Blue', '', 2),
				('3', 'A', 'Alpha', '', 3),])
	channel_RGB: bpy.props.EnumProperty(default='0',
		items= [('0', 'R', 'Red', '', 0),
				('1', 'G', 'Green', '', 1),
				('2', 'B', 'Blue', '', 2),])
	target_image: bpy.props.PointerProperty(type=bpy.types.Image, name='Target Image')


class IMT_PT_snapshot(bpy.types.Panel):
	bl_space_type = 'VIEW_3D'
	bl_region_type = 'HEADER'
	bl_label = "Snapshot"
	bl_ui_units_x = 13

	def draw(self, _context):
		layout = self.layout
		layout.label(text="Viewport Snapshot")
		layout.label(text="TODO: Use Render Border Toggle")
		layout.label(text="TODO: Save to Clipboard Toggle")
		layout.label(text="TODO: Open in Image Editor Toggle")

class IMT_PT_image_tools_panel(bpy.types.Panel):
	bl_label = "Image Tools"
	bl_space_type = "IMAGE_EDITOR"
	bl_region_type = 'UI'
	bl_category = "Image Tools"
	
	def draw(self, context):
		layout = self.layout
		settings = context.scene.IMT_Settings
		layout.operator("image_tools.flip_image")
		image = context.area.spaces.active.image

		layout.template_ID_preview(settings, 'target_image', new="image.new", open="image.open")
		if image.channels == 4:
			layout.prop(settings, 'channel_RGBA', expand=True)
		elif image.channels == 3:
			layout.prop(settings, 'channel_RGB', expand=True)
			settings.channel_RGBA = settings.channel_RGB
		layout.operator("image_tools.isolate_channel")
		layout.operator("image_tools.write_to_channel")
		layout.operator("image_tools.generate_sdf_approx")
		layout.operator("image_tools.generate_sdf")
		return


classes = [	IMT_PT_image_tools_panel,
			IMT_PT_snapshot,
			IMT_Settings,]

def register():
	for c in classes:
		bpy.utils.register_class(c)
	bpy.types.Scene.IMT_Settings = bpy.props.PointerProperty(type=IMT_Settings)
	bpy.types.VIEW3D_HT_header.append(draw_snapshot)

def unregister():
	for c in classes:
		bpy.utils.unregister_class(c)
	del bpy.types.Scene.IMT_Settings
