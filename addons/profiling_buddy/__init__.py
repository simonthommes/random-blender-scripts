import bpy
from . import modifier_profiling

modules = [modifier_profiling]

def register():
	for m in modules:
		m.register()

def unregister():
	for m in modules:
		m.unregister()
