import bpy

class PB_Modifier_Profiling_Panel(bpy.types.Panel):
    """
    """
    bl_label = "Modifier Profiling"
    bl_idname = "SCENE_PT_modifier_profiling"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "modifier"

    def draw(self, context):
        draw_modifier_times(self, context)

def time_to_string(t):
    ''' Formats time in seconds to the nearest sensible unit.
    '''
    units = {3600.: 'h', 60.: 'm', 1.: 's', .001: 'ms'}
    for factor in units.keys():
        if t >= factor:
            return f'{t/factor:.3g} {units[factor]}'
    if t >= 1e-4:
        return f'{t/factor:.3g} {units[factor]}'
    else:
        return f'<0.1 ms'

def draw_modifier_times(self, context):
    depsgraph = context.view_layer.depsgraph

    ob = context.object
    ob_eval = ob.evaluated_get(depsgraph)

    box = self.layout.box()
    times = []
    total = 0
    for mod_eval in ob_eval.modifiers:
        t = mod_eval.execution_time
        times += [t]
        total += t
    
    col_fl = box.column_flow(columns=2)
    col = col_fl.column(align=True)
    for mod_eval in ob_eval.modifiers:
        row = col.row()
        row.enabled = mod_eval.show_viewport
        row.label(text=f'{mod_eval.name}:')
    
    col = col_fl.column(align=True)
    for i, t in enumerate(times):
        row = col.row()
        row.enabled = ob_eval.modifiers[i].show_viewport
        row.alert = t >= 0.8*max(times)
        row.label(text=time_to_string(t))
    
    row = box.column_flow()
    row.column().label(text=f'TOTAL:')
    row.column().label(text=time_to_string(sum(times)))

classes = [
            PB_Modifier_Profiling_Panel,
            ]

def register():
    for c in classes:
        bpy.utils.register_class(c)

def unregister():
    for c in classes:
        bpy.utils.unregister_class(c)

if __name__ == "__main__":
    register()