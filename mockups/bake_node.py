import bpy

class BakeNodeOperator(bpy.types.Operator):
    """Bakes """
    bl_idname = "node.bake_node"
    bl_label = "Bake"

    @classmethod
    def poll(cls, context):
        space = context.space_data
        return space.type == 'NODE_EDITOR'

    def execute(self, context):
        bake_node = context.node
        node_tree = context.space_data.node_tree
        
        output_node_backup = node_tree.get_output_node('CYCLES')
        output_node_backup.is_active_output = False
        
        output_node = node_tree.nodes.new('ShaderNodeOutputMaterial')
        output_node.target = 'CYCLES'
        output_node.is_active_output = True
        
        texture_node = node_tree.nodes.new('ShaderNodeTexImage')
        texture_node.image = bake_node.node_tree.nodes.get("Image Texture").image
        
        # Connect bake node to output
        node_tree.links.new(bake_node.inputs[0].links[0].from_socket,output_node.inputs[0])
        
        # Bake to image
        node_tree.nodes.active = texture_node
        bpy.ops.object.bake({'selected_objects': [context.object]}, type='EMIT')
        
        # Disconnect and remove temporary output node
        output_node_backup.is_active_output = True
        output_node.is_active_output = False
        node_tree.links.remove(output_node.inputs[0].links[0])
        node_tree.nodes.remove(output_node)
        node_tree.nodes.remove(texture_node)
        bake_node.pinned = True
        
        return {'FINISHED'}

class FreeNodeOperator(bpy.types.Operator):
    """Frees"""
    bl_idname = "node.free_node"
    bl_label = "Free"

    @classmethod
    def poll(cls, context):
        space = context.space_data
        return space.type == 'NODE_EDITOR'

    def execute(self, context):
        node = context.node
        node_tree = context.space_data.node_tree
        
        node.pinned = False
        return {'FINISHED'}

class BakeNode(bpy.types.NodeCustomGroup):

    bl_name='BakeNode'
    bl_label='Dynamic Texture Bake'
    bl_width_default = 240

    # Manage the internal nodes to perform the chained operation - clear all the nodes and build from scratch each time.
    def __nodetree_setup__(self):

        # Remove all links and all nodes that aren't Group Input or Group Output
        self.node_tree.links.clear()
        for node in self.node_tree.nodes:
            if not node.name in ['Group Input','Group Output']:
                self.node_tree.nodes.remove(node)

        # Start from Group Input and add nodes as required
        groupinput = self.node_tree.nodes['Group Input']
        input_socket = self.node_tree.inputs.new("NodeSocketColor", "Color")
        output_socket = self.node_tree.outputs.new("NodeSocketColor", "Color")
        input_socket.hide_value = True
        
        uv_map_node = self.node_tree.nodes.new('ShaderNodeUVMap')
        texture_node = self.node_tree.nodes.new('ShaderNodeTexImage')
        self.node_tree.links.new(uv_map_node.outputs[0],texture_node.inputs[0])

        # Connect the input to the output
        self.node_tree.links.new(self.node_tree.nodes['Group Input'].outputs[0],self.node_tree.nodes['Group Output'].inputs[0])
    
    # Update nodetree depening on node state
    def __nodetree_update__(self):
        texture_node = self.node_tree.nodes.get("Image Texture")
        input_node = self.node_tree.nodes.get("Group Input")
        output_node = self.node_tree.nodes.get("Group Output")
        self.node_tree.links.remove(output_node.inputs[0].links[0])
        
        if self.pinned:
            self.node_tree.links.new(texture_node.outputs[0],output_node.inputs[0])
        else:
            self.node_tree.links.new(input_node.outputs[0],output_node.inputs[0])
    
    def update_pinning(self, context):
        self.__nodetree_update__()
    
    # The node properties
    pinned: bpy.props.BoolProperty(default=False, update=update_pinning)
    
    # Setup the node - setup the node tree and add the group Input and Output nodes
    def init(self, context):
        self.node_tree=bpy.data.node_groups.new(self.bl_name, 'ShaderNodeTree')
        if hasattr(self.node_tree, 'is_hidden'):
            self.node_tree.is_hidden=True
        self.node_tree.nodes.new('NodeGroupInput')
        self.node_tree.nodes.new('NodeGroupOutput')
        self.__nodetree_setup__()

    # Draw the node components
    def draw_buttons(self, context, layout):
        if not self.pinned:
            layout.operator('node.bake_node')
        else:
            layout.operator('node.free_node')
        row=layout.row()
        # relevant nodes
        texture_node = self.node_tree.nodes.get("Image Texture")
        uv_node = self.node_tree.nodes.get("UV Map")
        if texture_node.image == None or not self.pinned:
            layout.template_ID(texture_node, 'image', new="image.new", open="image.open")
        else:
            layout.template_ID_preview(texture_node, 'image', new="image.new", open="image.open")
        # texture settings
        layout.prop(texture_node, 'interpolation', text='',)
        column = layout.column(align=True)
        column.alignment = 'RIGHT'
        if not texture_node.image == None:
            column.prop(texture_node.image.colorspace_settings, 'name', text='Color Space')
        # uv map field
        layout.prop_search(uv_node, 'uv_map', context.object.data, "uv_layers")

    # Copy
    def copy(self, node):
        self.node_tree=node.node_tree.copy()

    # Free (when node is deleted)
    def free(self):
        bpy.data.node_groups.remove(self.node_tree, do_unlink=True)


from nodeitems_utils import NodeItem, register_node_categories, unregister_node_categories
from nodeitems_builtins import ShaderNodeCategory

classes = [ BakeNode,
            BakeNodeOperator,
            FreeNodeOperator,]

def register():
    for c in classes:
        bpy.utils.register_class(c)
    newcatlist = [ShaderNodeCategory("SH_CUSTOM", "Custom Nodes", items=[NodeItem("BakeNode"),]), ]
    #bpy.types.NODE_MT_category_SH_NEW_TEXTURE.append(newcatlist)
    register_node_categories("CUSTOM_NODES", newcatlist)

def unregister():
    unregister_node_categories("CUSTOM_NODES")
    for c in classes:
        bpy.utils.unregister_class(c)

try :
    unregister()
except:
    pass
register() 
