from typing import Any, Dict, List, Set, Union, Optional

import bpy
import mathutils
import bmesh
import numpy as np

def copy_attributes(a: Any, b: Any) -> None:
    keys = dir(a)
    for key in keys:
        if (
            not key.startswith("_")
            and not key.startswith("error_")
            and key != "group"
            and key != "is_valid"
            and key != "rna_type"
            and key != "bl_rna"
        ):
            try:
                setattr(b, key, getattr(a, key))
            except AttributeError:
                pass


def copy_driver(
    source_fcurve: bpy.types.FCurve,
    target_obj: bpy.types.Object,
    data_path: Optional[str] = None,
    index: Optional[int] = None,
) -> bpy.types.FCurve:
    if not data_path:
        data_path = source_fcurve.data_path

    new_fc = None
    try:
        if index:
            new_fc = target_obj.driver_add(data_path, index)
        else:
            new_fc = target_obj.driver_add(data_path)
    except:
        print(f"Couldn't copy driver {source_fcurve.data_path} to {target_obj.name}")
        return

    copy_attributes(source_fcurve, new_fc)
    copy_attributes(source_fcurve.driver, new_fc.driver)

    # Remove default modifiers, variables, etc.
    for m in new_fc.modifiers:
        new_fc.modifiers.remove(m)
    for v in new_fc.driver.variables:
        new_fc.driver.variables.remove(v)

    # Copy modifiers
    for m1 in source_fcurve.modifiers:
        m2 = new_fc.modifiers.new(type=m1.type)
        copy_attributes(m1, m2)

    # Copy variables
    for v1 in source_fcurve.driver.variables:
        v2 = new_fc.driver.variables.new()
        copy_attributes(v1, v2)
        for i in range(len(v1.targets)):
            copy_attributes(v1.targets[i], v2.targets[i])

    return new_fc


def copy_drivers(source_ob: bpy.types.Object, target_ob: bpy.types.Object) -> None:
    """Copy all drivers from one object to another."""
    if not hasattr(source_ob, "animation_data") or not source_ob.animation_data:
        return

    for fc in source_ob.animation_data.drivers:
        copy_driver(fc, target_ob)


def closest_edge_on_face_to_line(face, p1, p2, skip_edges=None):
    ''' Returns edge of a face which is closest to line.'''
    for edge in face.edges:
        if skip_edges:
            if edge in skip_edges:
                continue
        res = mathutils.geometry.intersect_line_line(p1, p2, *[edge.verts[i].co for i in range(2)])
        if not res:
            continue
        (p_traversal, p_edge) = res
        frac_1 = (edge.verts[1].co-edge.verts[0].co).dot(p_edge-edge.verts[0].co)/(edge.verts[1].co-edge.verts[0].co).length**2.
        frac_2 = (p2-p1).dot(p_traversal-p1)/(p2-p1).length**2.
        if (frac_1 >= 0 and frac_1 <= 1) and (frac_2 >= 0 and frac_2 <= 1):
            return edge
    return None

def interpolate_data_from_face(bm_source, tris_dict, face, p, data_layer_source, data_suffix = ''):
    ''' Returns interpolated value of a data layer within a face closest to a point.'''

    (tri, point) = closest_tri_on_face(tris_dict, face, p)
    if not tri:
        return None
    weights = mathutils.interpolate.poly_3d_calc([tri[i].vert.co for i in range(3)], point)

    if not data_suffix:
        cols_weighted = [weights[i]*np.array(data_layer_source[tri[i].index]) for i in range(3)]
        col = sum(np.array(cols_weighted))
    else:
        cols_weighted = [weights[i]*np.array(getattr(data_layer_source[tri[i].index], data_suffix)) for i in range(3)]
        col = sum(np.array(cols_weighted))
    return col

def closest_face_to_point(bm_source, p_target, bvh_tree = None):
    if not bvh_tree:
        bvh_tree = mathutils.bvhtree.BVHTree.FromBMesh(bm_source)
    (loc, norm, index, distance) = bvh_tree.find_nearest(p_target)
    return bm_source.faces[index]

def tris_per_face(bm_source):
    tris_source = bm_source.calc_loop_triangles()
    tris_dict = dict()
    for face in bm_source.faces:
        tris_face = []
        for i in range(len(tris_source))[::-1]:
            if tris_source[i][0] in face.loops:
                tris_face.append(tris_source.pop(i))
        tris_dict[face] = tris_face
    return tris_dict

def closest_tri_on_face(tris_dict, face, p):
    points = []
    dist = []
    tris = []
    for tri in tris_dict[face]:
        point = mathutils.geometry.closest_point_on_tri(p, *[tri[i].vert.co for i in range(3)])
        tris.append(tri)
        points.append(point)
        dist.append((point-p).length)
    min_idx = np.argmin(np.array(dist))
    point = points[min_idx]
    tri = tris[min_idx]
    return (tri, point)

def transfer_shapekeys_proximity(obj_source, obj_target) -> None:
    '''
    Transfers shapekeys from one object to another
    based on the mesh proximity with face interpolation.
    '''
    # copy shapekey layout
    if not obj_source.data.shape_keys:
        return
    for sk_source in obj_source.data.shape_keys.key_blocks:
        if obj_target.data.shape_keys:
            sk_target = obj_target.data.shape_keys.key_blocks.get(sk_source.name)
            if sk_target:
                continue
        sk_target = obj_target.shape_key_add()
        sk_target.name = sk_source.name
    for sk_target in obj_target.data.shape_keys.key_blocks:
        sk_source = obj_source.data.shape_keys.key_blocks[sk_target.name]
        sk_target.vertex_group = sk_source.vertex_group
        sk_target.relative_key = obj_target.data.shape_keys.key_blocks[sk_source.relative_key.name]

    bm_source = bmesh.new()
    bm_source.from_mesh(obj_source.data)
    bm_source.faces.ensure_lookup_table()

    bvh_tree = mathutils.bvhtree.BVHTree.FromBMesh(bm_source)

    tris_dict = tris_per_face(bm_source)

    for i, vert in enumerate(obj_target.data.vertices):
        p = vert.co
        face = closest_face_to_point(bm_source, p, bvh_tree)

        (tri, point) = closest_tri_on_face(tris_dict, face, p)
        if not tri:
            continue
        weights = mathutils.interpolate.poly_3d_calc([tri[i].vert.co for i in range(3)], point)

        for sk_target in obj_target.data.shape_keys.key_blocks:
            sk_source = obj_source.data.shape_keys.key_blocks.get(sk_target.name)

            vals_weighted = [weights[i]*(sk_source.data[tri[i].vert.index].co-obj_source.data.vertices[tri[i].vert.index].co) for i in range(3)]
            val = mathutils.Vector(sum(np.array(vals_weighted)))
            sk_target.data[i].co = vert.co+val

context = bpy.context
obj_source = context.active_object
selected = context.selected_objects[:]
selected.remove(obj_source)

for obj_target in selected:

    # transfer shapekeys
    transfer_shapekeys_proximity(obj_source, obj_target)

    # transfer drivers
    copy_drivers(obj_source.data.shape_keys, obj_target.data.shape_keys)