import bpy
import mathutils

def get_weight(vg, index):
    try:
        return vg.weight(index)
    except:
        return 0
    

def transfer_data(ob_from, ob_to):
    '''
    Transfers data (weights, shapekeys) from one object to another
    based on the mesh proximity with face interpolation.
    '''
    # copy shapekey layout
    if ob_from.data.shape_keys:
        for sk_from in ob_from.data.shape_keys.key_blocks:
            if ob_to.data.shape_keys:
                sk_to = ob_to.data.shape_keys.key_blocks.get(sk_from.name)
                if sk_to:
                    continue
            sk_to = ob_to.shape_key_add()
            sk_to.name = sk_from.name
        for sk_to in ob_to.data.shape_keys.key_blocks:
            sk_from = ob_from.data.shape_keys.key_blocks[sk_to.name]
            sk_to.relative_key = ob_to.data.shape_keys.key_blocks[sk_from.relative_key.name]
        
    for vg_from in ob_from.vertex_groups:
        vg_to = ob_to.vertex_groups.get(vg_from.name)
        if vg_to:
            ob_to.vertex_groups.remove(vg_to)
        ob_to.vertex_groups.new(name=vg_from.name)

    # build kd tree
    kd_from = mathutils.kdtree.KDTree(len(ob_from.data.vertices))
    for i, f in enumerate(ob_from.data.polygons):
        kd_from.insert(f.center, i)
    kd_from.balance()

    # transfer vertex data
    for v in ob_to.data.vertices:
        co, index, dist = kd_from.find(v.co)
        f = ob_from.data.polygons[index]
        p = v.co + f.normal*(f.normal.dot(co - v.co))
        
        
        indices = f.vertices
        weights = []
        match = -1
        for i, idx in enumerate(indices):
            d = (p - ob_from.data.vertices[idx].co).length
            if d == 0:
                match = i
                break
            else:
                weights.append(1./d)
        if match >= 0:
            weights = [float(i == match) for i in range(len(indices))]
        
        total = sum(weights)
        weights = [w/total for w in weights]
        
        # shapekey transfer
        if ob_from.data.shape_keys:
            for sk in ob_to.data.shape_keys.key_blocks:
                if sk.relative_key == sk:
                    continue
                sk_from = ob_from.data.shape_keys.key_blocks[sk.name]
                sk_interpolated = mathutils.Vector((0,0,0))
                for i, w in zip(indices, weights):
                    sk_interpolated += (sk_from.data[i].co-sk_from.relative_key.data[i].co)*w
                sk_interpolated += sk.relative_key.data[v.index].co
                sk.data[v.index].co = sk_interpolated
        
        # vertex weights transfer
        for vg in ob_to.vertex_groups:
            vg_from = ob_from.vertex_groups.get(vg.name)
            if not vg_from:
                continue
            vg_interpolated = 0
            for i, w in zip(indices, weights):
                vg_interpolated += get_weight(vg_from, i)*w
            if vg_interpolated==0:
                continue
            vg.add([v.index], vg_interpolated, 'REPLACE')

context = bpy.context
ob_from = context.active_object
selected = context.selected_objects[:]
selected.remove(ob_from)

for ob_to in selected:
    transfer_data(ob_from, ob_to)
